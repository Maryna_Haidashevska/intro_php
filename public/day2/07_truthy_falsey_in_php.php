<?php

// in JS
// '0' as string in JS... TRUTHY
// 0 FALSEY
// '' FALSEY
// ' ' TRUTHY
// NULL FALSEY
// UNDEFINED FALSEY
// TRUE  TRUE
// FALSE FALSE

// in PHP
// '0' as string in PHP... FALSEY
// 0 as integer is FLASEY
// !0 in TRUTHY (including negative numbers)
// ' ' (string with space) TRUTHY
// '' (empty string) FALSEY
// [] empty array is FALSEY
// NULL FALSEY
// TRUE TRUE
// FALSE FALSE

