<?php

/**
 * Escape string for general use in HTML
 * @param  String $string data to be sanitized
 * @return String
 */
function e($string) {
	return htmlentities($string, null, 'UTF-8');
}

/**
 * Escape string for use in attribute (quotes entitized)
 * @param  String $string data to be sanitized
 * @return String 
 */
function e_attr($string) {
	return htmlentities($string, ENT_QUOTES, 'UTF-8');
}

$name = 'Bill';
$email = 'bill@hotmail.com';
$phone = '123-346-2345';
$class = 'active';

?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="" />
	<title>Escaping Output</title>
</head>
<body>

<h1>Escaping Output</h1>

<!-- Data must be sanitized for use in specific contexts:
	-- as an HTML attribute
	-- as a value in HTML
-->
<p class="<?=e_attr($class)?>">Username: <?=e($name)?> <br />
	Email: <?=e($email)?> <br />
	Phone: <?=e($phone)?></p>
</body>
</html>
