<?php

require 'functions.php';

// When testing conditions, put the constant value first, so that
// the if condition will generate an error if we inadvertently
// attempt to assign a value to it.
// if($_SERVER['REQUEST_METHOD'] != 'POST') {
   if('POST' != $_SERVER['REQUEST_METHOD']) {
	// die kills script in its track at this line:
	die('Please use our registration form!');

	// exit is an alias for die
	// exit('Please use our registration form!');
}

// required or output to error

// start with an empty $errors array
// if there's an error, we set the error
// in the array, with the key = the fieldname

// empty errors array
$errors = [];

// list of required fields
$required = ['first_name', 'last_name', 'email'];

// loop through $_POST
/*
foreach($_POST as $key => $value) {
	if(empty($value)) {
	// if field is empty add required error message
		$errors[$key] = "$key is a required field";
	}
}
// end loop
*/

// Test only if required fields are empty
// if so, set an error message
foreach($required as $keyname) {
	if(empty($_POST[$keyname])) {
		$errors[$keyname] = "$keyname is a required field";
	}
}

/*
echo '<pre>';
var_dump($_POST);
var_dump($errors);
var_dump($_SERVER);

*/


// SEE BELOW FOR ALTERNATIVE PHP SYNTAX

?><!DOCTYPE html>
<html>
<head>
	<title>Thank you!</title>
</head>
<body>

	<?php 
		if($errors) {
			echo '<h1>We found errors</h1>';
			echo '<div class="errors">';
			  echo "<p>Please go back and correct the following errors:</p>";
				echo '<ul>';
					foreach($errors as $value) {
						echo "<li>$value</li>";
					}
				echo '</ul>';
			echo "</div>";
		} else {
			// show user
		} 
	?>

	<?php if($errors) : ?>
		<h1>We found errors</h1>
		<div class="errors">
			<p>Please go back and correct the following errors:</p>
			<ul>
			<?php foreach($errors as $value) : ?>
				<li><?=$value?></li>
			<?php endforeach; ?>
			</ul>
		</div>
	<?php else : ?>
		<h1>Thank you</h1>
		<h2>your info:</h2>

		<p>
		<?php foreach($_POST as $key => $value) : ?>
			<strong><?=e($key)?></strong>: <?=e($value)?><br />
		<?php endforeach; ?>
		</p>

	<?php endif; ?>

</body>
</html>