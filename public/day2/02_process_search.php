<?php




echo 'It works!';

// Since our form is being submitted using the GET
// method, we can access its values in the $_GET
// SuperGlobal
// var_dump($_GET);
// Other SuperGlobals include:
// $_POST -- for POST submission
// $_COOKE -- for cookies set in the browser
// $_REQUEST -- ALL other SuperGLobals, combined... do not
// use this one!
// 
// htmlentities()
// htmlspecialchars()

// sanitize $_GET vars for output
// basic output
$q = htmlentities($_GET['q']);
$email = htmlentities($_GET['email']);

// what is encoding of website?  We need to specify.
// The third parameter is the character set.  You should
// always define it
$q = htmlentities($_GET['q'], ENT_QUOTES, 'UTF-8');
$email = htmlentities($_GET['email'], ENT_QUOTES, 'UTF-8');

echo "<p>You searched for: {$q}</p>";

echo "<p>Your email address is: {$email}</p>";

$class ="'active";

$class = htmlentities($class, ENT_QUOTES, "UTF-8");

// When using potentially unsafe  values as HTML
// attributes, we need to escape a little bit differently.
echo "<div class='$class'>My content</div>";
