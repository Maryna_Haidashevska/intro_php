<?php

// Two kinds of 'empty' in PHP
// in js you would consider UNDEFINED
// in js test for length

$name = 'Steve';

$last_name = '';

// in PHP conditionals work exactly the same as they do in JS

// Test if name is set


// isset() returns true if a variable has been declared, even if it has no value

// empty() returns true if a variable has not been declared, or if it has been
// declared but has no value

if(isset($name)) {
	echo '<p>$name is set</p>';
} else {
	echo '<p>$name is not set</p>';
}

if(isset($email)) {
	echo '<p>$email is set</p>';
} else {
	echo '<p>$email is not set</p>';
}

if(isset($last_name)) {
	echo '<p>$last_name is set</p>';
} else {
	echo '<p>$last_name is not set</p>';
}

if(empty($name)) {
	echo '<p>$name is empty (it has no value)</p>';
} else {
	echo '<p>$name is not empty (it has a value)</p>';
}

if(empty($email)) {
	echo '<p>$email is empty (it has no value)</p>';
} else {
	echo '<p>$email is not empty (it has a value)</p>';
}

if(empty($last_name)) {
	echo '<p>$last_name is empty (it has no value)</p>';
} else {
	echo '<p>$last_name is not empty (it has a value)</p>';
}