<?php

// get out connection
require 'connect.php';
// create a query

if(!empty($_GET['s'])){
$query = 'SELECT 
                book.book_id,
                book.title,
                author.name as author,
                book.year_published,
                book.num_pages,
                genre.name as genre,
                book.price
                FROM
                book
                JOIN author USING(author_id)
                JOIN genre USING (genre_id)
                WHERE
                title LIKE '%text%'
                ORDER by book.title';

                $params = array(':search' => "'%$_GET['s'])%'";
} else{
	$query = 'SELECT 
                book.book_id,
                book.title,
                author.name as author,
                book.year_published,
                book.num_pages,
                genre.name as genre,
                book.price
                FROM
                book
                JOIN author USING(author_id)
                JOIN genre USING (genre_id)
                ORDER by book.title';

                $params = [];
}


// create statement

$stmt = $dbh->prepare($query);

// fetch our results
$stmt->execute($params);
$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
//var_dump($results);
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="" />
	<title>Book List</title>
	<style>
		table{
			border-collapse: collapse;
		}
		td, th {
			border: 1px solid #cfcfcf;
			padding: 6px 10px;
			text-align: left;
		}
	</style>
</head>
<body>

<h1>Book List</h1>

<form action="<?=basename($_SERVER['PHP_SELF'])?>" method="get">
	<p><input type="text" name="s" /><button>Search</button></p>
</form>
<table>
	<tr>
		<th>Title</th>
		<th>Author</th>
		<th>Year Published</th>
		<th>Num Pages</th>
		<th>Genre</th>
		<th>Price</th>
	</tr>
	
    <?php foreach ($results as $key => $row) : ?>
    <tr>
		<td><a href="03_detail_view.php?book_id=<?=$row['book_id']?>"><?=$row['title']?></a></td>
		<td><?=$row['author']?></td>
		<td><?=$row['year_published']?></td>
		<td><?=$row['num_pages']?></td>
		<td><?=$row['genre']?></td>
		<td><?=$row['price']?></td>
    </tr>
    <?php endforeach; ?>
</table>


</body>
</html>