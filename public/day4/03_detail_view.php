<?php

if(empty($_GET['book_id'])) die('Please pick a book');

$book_id = intval($_GET['book_id']);

require 'connect.php';
require 'functions.php';

$query = 'SELECT book.*,
                author.name as author,
                author.country as author_country,
                format.name as format,
                genre.name as genre,
                publisher.name as publisher,
                publisher.city as publisher_city
                FROM
                book
                JOIN author USING(author_id)
                JOIN publisher USING(publisher_id)
                JOIN format USING(format_id)
                JOIN genre USING (genre_id)
                WHERE 
                book.book_id = :book_id';
               
$params = [':book_id' => $book_id];

$stmt = $dbh->prepare($query);  

$stmt->execute($params);

$result = $stmt->fetch(PDO::FETCH_ASSOC);

// var_dump($result);


?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="" />
	<title>Book Detail</title>
</head>
<body>

<h1>Book Detail</h1>

<h2>Dune</h2>

<ul>
<li><strong>Title</strong> : <?=$result['title']?></li>
<li><strong>Author</strong> : <?=$result['author']?></li>
<li><strong>Year Published</strong> : <?=$result['year_published']?></li>
<li><strong>Number of pages</strong> : <?=$result['num_pages']?></li>
<li><strong>In print</strong>: <?=($result['in_print'])? 'yes' : 'no'?></li>
<li><strong>Pricee</strong>: $<?=$result['price']?></li>
<li><strong>Genre</strong>: <?=$result['genre']?></li>
<li><strong>Format</strong> : <?=$result['format']?></li>
</ul>

<h3>Author</h3>
<ul>
<li><strong>Author</strong>: <?=$result['author']?></li>
<li><strong>Publisher Country</strong>: <?=$result['author_country']?></li>
</ul>

<h3>Publisher</h3>
<ul>
<li><strong>Publisher</strong>: <?=$result['publisher']?></li>
<li><strong>Publisher City</strong>: <?=$result['publisher_city']?></li>
</ul>

</body>
</html>