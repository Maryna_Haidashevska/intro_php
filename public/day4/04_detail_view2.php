<?php

if(empty($_GET['book_id'])) die('Please pick a book');

$book_id = intval($_GET['book_id']);

require 'connect.php';
require 'functions.php';

$query = 'SELECT book.*,
                author.name as author,
                author.country as author_country,
                format.name as format,
                genre.name as genre,
                publisher.name as publisher,
                publisher.city as publisher_city
                FROM
                book
                JOIN author USING(author_id)
                JOIN publisher USING(publisher_id)
                JOIN format USING(format_id)
                JOIN genre USING (genre_id)
                WHERE 
                book.book_id = :book_id';
               
$params = [':book_id' => $book_id];

$stmt = $dbh->prepare($query);  

$stmt->execute($params);

$result = $stmt->fetch(PDO::FETCH_ASSOC);

// var_dump($result);


?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="" />
	<title>Book Detail</title>
</head>
<body>

<p><a href="02_list_view.php">Back to book list</a></p>

<h1>Book Detail with loop</h1>

<h2><?=$result['title']?></h2>

<ul>
<?php foreach($result as $key =>$value) : ?>
<li><strong><?=label($key)?></strong> : <?=$value?></li>
<?php endforeach; ?>
</ul>

</body>
</html>