<?php

define('DB_USER', 'web_user');
define('DB_PASS', 'mypass');
define('DB_DSN', 'mysql:host=localhost;dbname=books');

$dbh = new PDO(DB_DSN, DB_USER, DB_PASS);
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


