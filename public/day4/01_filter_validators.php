<?php

$errors = [];
$success = false;

include 'functions.php';


//filter_input
// test for POST
if('POST' == $_SERVER['REQUEST_METHOD']){

if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
	$errors['email'] = 'Please provide a valid email address';
}
if(!filter_var($_POST['age'], FILTER_VALIDATE_INT)){
	$errors['age'] = 'Age must be an integer';
} //elseif($_POST['age']) < 18 {
	//$errors['age'] = 'You must be at least 18 years old';}
if(!filter_var($_POST['price'], FILTER_VALIDATE_FLOAT)){
	$errors['price'] = 'Price must be a real number';
}
}
// end POST
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="" />
	<title>PHP Validation Filters</title>
	<style>
        .errors, .error{
        	color: #900;
        }
    </style>
</head>
<body>

<h1>PHP Validation Filters</h1>

<?php include __DIR__ . '/errors.inc.php'?>
<!-- basename trims a file path to the base filename. without basename, $_SERVER['PHP_SELF'] -->
<form action="<?=$_SERVER['PHP_SELF']?>" method="post" novalidate>
<fieldset>
	<legend>Testing Filters</legend>
	<p>
		<label for="email">Email</label><br />
		<input type="email" name="email" id="email" value="<?=clean('email')?>" />
		<?=(isset($errors['email'])) ? "<span class='error'>{$errors['email']}</span>" : '' ?>
	</p>

	<p>
		<label for="age">Age</label><br />
		<input type="text" name="age" id="age" value="<?=clean('age')?>" />
		<?php
		  if(isset($errors['age'])) echo "<span class='error'>{$errors['age']}</span>" ?>
	</p>
    
    <p>
		<label for="price">Price</label><br />
		<input type="text" name="price" id="price" value="<?=clean('price')?>" />
		<?php
		  if(isset($errors['price'])) echo "<span class='error'>{$errors['price']}</span>" ?>
	</p>
   <p><input type="submit" name="submit" /></p>
</fieldset>
</form>
</body>
</html>