<?php

$title = 'My Web Page';
$body_id = 'home';

function esc($string){
	return htmlentities($string, null, "UTF-8")
}

function esc_attr($string){
	return htmlentities($string, ENT_QUOTES, "UTF-8")
}
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="" />
	<title><?=esc($title)></title>
</head>
<body id="<?=esc_attr($title)?>">

<h1><?=esc($title)?></h1>



</body>
</html>