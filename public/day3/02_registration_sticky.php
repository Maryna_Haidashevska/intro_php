<?php



$errors = [];

$success = false;

if('POST' == $_SERVER['REQUEST_METHOD']){
	
	$required = ['first_name', 'last_name', 'email'];

	foreach ($required as $key => $value) {
		if(empty($_POST[$value])) {
			$errors[$value] = $value. 'is a required field';
		}
	}

	if(!$errors) {
        $success = true;
	}
}


?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="" />
	<title>Registration Form</title>
</head>
<body>

<h1>Registration</h1>

<?php if($errors) : ?>
		<h1>We found errors</h1>
		<div class="errors">
			<p>Please go back and correct the following errors:</p>
			<ul>
			<?php 
			foreach($errors as $value) : ?>
				<li><?=$value?></li>
			<?php endforeach; ?>
			</ul>
		</div>

	<?php endif; ?>

<?php if(!$success) : ?>

<form method="post" action="02_registration_sticky.php" novalidate>
<fieldset>
	<legend>Register</legend>
	<p><label for="first_name">First Name</label><br />
		<input type="text" name="first_name" value="<?php echo (!empty($_POST['first_name'])) ? htmlentities($_POST['first_name'], ENT_QUOTES, "UTF-8") : '' ; ?>" /></p>
	<p><label for="last_name">Last Name</label><br />
		<input type="text" name="last_name" value="<?php echo (!empty($_POST['last_name'])) ? htmlentities($_POST['last_name'], ENT_QUOTES, "UTF-8") : '' ; ?>"/></p>
	<p><label for="street">Street</label><br />
		<input type="text" name="street" value="<?php echo (!empty($_POST['street'])) ? htmlentities($_POST['street'], ENT_QUOTES, "UTF-8") : '' ; ?>" /></p>
	<p><label for="city">City</label><br />
		<input type="text" name="city" value="<?php echo (!empty($_POST['city'])) ? htmlentities($_POST['city'], ENT_QUOTES, "UTF-8") : '' ; ?>"/></p>
	<p><label for="province">Province</label><br />
		<input type="text" name="province" value="<?php echo (!empty($_POST['province'])) ? htmlentities($_POST['province'], ENT_QUOTES, "UTF-8") : '' ; ?>" /></p>
	<p><label for="postal_code">Postal Code</label><br />
		<input type="text" name="postal_code" value="<?php echo (!empty($_POST['postal_code'])) ? htmlentities($_POST['postal_code'], ENT_QUOTES, "UTF-8") : '' ; ?>" /></p>
	<p><label for="email">Email</label><br />
		<input type="text" name="email" value="<?php echo (!empty($_POST['email'])) ? htmlentities($_POST['email'], ENT_QUOTES, "UTF-8") : '' ; ?>"/></p>
	<p><label for="phone">Phone</label><br />
		<input type="text" name="phone" value="<?php echo (!empty($_POST['phone'])) ? htmlentities($_POST['phone'], ENT_QUOTES, "UTF-8") : '' ; ?>" /></p>
    <p><label for="interests"><br />
       <input type="checkbox" name="interests[]" value="hockey"
       <?php 
       if(!empty($_POST['interests']) && in_array('hockey', $_POST['interests'])) {
       	echo 'checked';
       }
       ?> />&nbsp;Hockey<br />
       </label>
    	
    </p>
	<p><button>Submit</button></p>
</fieldset>
</form>

<?php else : ?>

<h2>Thank you for the registration</h2>	
<ul>
<?php foreach ($_POST as $key => $value): ?>
    <li><strong><?=e($key)?></strong>: <?=e($value)?></li>
   <?php else : ?>
   	<li><strong><?=e($key)?></strong>
   		<ul>
   			<?php foreach ($value as $num => $interests) : ?>
   				# code...
   			}
<?php endforeach; ?>
</ul>
<?php endif; ?>


<pre>
  <?php //print_r($_SERVER) ?>
</pre>
</body>
</html>