<?php

$errors = [];

require 'functions.php';
require 'connect.php';
//if REQUEST METHOD IS POST
  // var dump POST
// end If POST

if('POST' == $_SERVER['REQUEST_METHOD']) {
	//var_dump($_POST);
	if(empty($_POST['name'])){
		$errors['name'] = 'Name is a required field';
	} elseif(strlen($_POST['name']) < 4){
	    $errors['name'] = 'Name is a required 4 characters in a field';
    }
	if(empty($_POST['country'])){
		$errors['country'] = 'Country is a required field';
	} elseif(strlen($_POST['name']) < 4){
	    $errors['name'] = 'Country is a required 2 characters in a field';
    
    }

	// create query
	if(!$errors){
		try{
			$query = "INSERT INTO 
			          author 
			          (name, country) 
			          VALUES
			           (:name, :country)";

			// prepare query
			$stmt = $dbh->prepare($query);

			$params = array(
			         ':name' => $_POST['name'],
			         ':country' => $_POST['country']
			     );
			$stmt->execute($params);

			$author_id = $dbh->lastInsertId();
			header('Location: 05_author_detail.php?author_id=' . $author_id);
			die;

	    } catch(Exception $e){
		    echo $e->getMessage();
		}
	}
}


?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="" />
	<title>Add Country</title>
    <style>
    	.errors{
    		color: #f00;
    	}
    </style>
</head>
<body>

<h1>Add a Country</h1>

<?php if($errors) : ?>
	<ul>
		<?php foreach ($errors as $key => $value) :?>
		<li class="errors"><?=e($value)?></li>
		<?php endforeach; ?> 
	</ul>
<?php endif; ?>

<form action="<?=$_SERVER['PHP_SELF'] ?>" method="post" novalidate>
    <fieldset>
    	<legend> Add New Author</legend>
	<p>
	<label for="author">Author</label>
	<input type="text" name="name" id="name" value="" />
	</p>

	<p>
	<label for="country">Country</label>
	<input type="text" name="country" id="country" value="" />
	</p>

	<p>
	<button>Submit</button>
	</p>
</fieldset>
</form>

</body>
</html>