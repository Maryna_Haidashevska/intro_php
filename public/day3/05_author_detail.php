<?php

if(empty($_GET['author_id'])){
	die('Author id required');
}

$id = intval($_GET['author_id']);
require 'functions.php';
require 'connect.php';

// Create a query to select an author based on id
$query = "SELECT *FROM author WHERE author_id = :author_id";

// prerape the query
$stmt = $dbh->prepare($query);

// prepare params array
$params = array(
        ':author_id' => $id
);

//execute the query
$stmt->execute($params);

// get the result
$result = $stmt->fetch(PDO::FETCH_ASSOC);

var_dump($result);
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="" />
	<title></title>
</head>
<body>

<?php if($result) :?>

<h1><?=$result['name']?></h1>

<ul>
    <?php foreach ($result as $key => $value) : ?>
        <li><strong></strong><?=$key?></strong> : <?=$value?></li> 
        <?php endforeach; ?>
</ul>

<p><a href="04_first_insert.php">Add another author</a></p>

<?php else : ?>


 
</body>
</html>