<?php

?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="" />
	<title>Registration Form</title>
</head>
<body>

<h1>Registration</h1>

<!--
action: process_registration.php
text - first_name
text - last_name
email/text - email
text - phone
text - street
text - city
text - postal_code
checkboxes - interests (4 or 5)
-->
<form method="post" action="03_process_registration.php" novalidate>
<fieldset>
	<legend>Register</legend>
	<p><label for="first_name">First Name</label><br />
		<input type="text" name="first_name" /></p>
	<p><label for="last_name">Last Name</label><br />
		<input type="text" name="last_name" /></p>
	<p><label for="street">Street</label><br />
		<input type="text" name="street" /></p>
	<p><label for="city">City</label><br />
		<input type="text" name="city" /></p>
	<p><label for="province">Province</label><br />
		<input type="text" name="province" /></p>
	<p><label for="postal_code">Postal Code</label><br />
		<input type="text" name="postal_code" /></p>
	<p><label for="email">Email</label><br />
		<input type="text" name="email" /></p>
	<p><label for="phone">Phone</label><br />
		<input type="text" name="phone" /></p>

	<p><button>Submit</button></p>
</fieldset>
</form>

</body>
</html>