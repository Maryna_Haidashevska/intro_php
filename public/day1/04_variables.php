<?php

// All variables in PHP start with a dollar sigh
// followed by a-zA-Z 0r _;
// followed by a-zA-Z0-9_
// 
// // some legal varuables names
show_source(__FILE__);

$name = 'Maryna';
$_email = 'marynahaidashevska@gmail.com';
$_1AEzX_ = 'test';

$name = 'Maryna';
$Name = 'Maryna';

// $1name = "Tom"; // illegal because 1 immediatelly followed $
// name = "DAve"; // no dollar sigh
// $first-name = 'Ken'; //  - not allowed 
?><!DOCTYPE html>
<html>
<head>
	<title>Using PHP Tags</title>
</head>
<body>
  <h1>This is a PHP file</h1>
</body>
</html>