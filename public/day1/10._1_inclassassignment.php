<?php

$user = [];
$user['first_name'] = 'Maryna';
$user['last_name'] = 'Haidashevska';
$user['email'] = 'marynahaidashevska@gmail.com';
$user['phone'] = '204-509-5915';
$user['street'] = 'Winnipeg';
$user['country'] = 'Canada';
$user['province'] = 'Manitoba';
$user['postal_code'] = 'R3M 2K7';

?><!DOCTYPE html>
<html>

<head>
	<title>In class Assignment </title>
	<h1>User Information</h1>
	<ul>
		<?php
        foreach($user as $key =>$value){
        	echo "<li><strong>$key:</strong> {$value}</li>";
        }
		?>
	</ul>
</head>
<body>
  <h1></h1>
</body>
</html>