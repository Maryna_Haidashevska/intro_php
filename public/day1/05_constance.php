<?php

// constance in php are defined using define() function
show_source(__FILE__);
// Once defined, constant values cannot be changed
// // NOte: it's a convention
define('GST', 0.5);
define('PST', 0.6);

// once defined can be used without a $ sigh


?><!DOCTYPE html>
<html>
<head>
	<title>PHP Constance</title>
</head>
<body>

  <h1>PHP Constance</h1>

  <p>The current PST rate is: <?php 
  
  // when referencing constnce no $ sign
  echo PST; ?></p>
</body>
</html>