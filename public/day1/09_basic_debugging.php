<?php

$myarray = [1,2,3,4,5];

$myarray2 = array('dog', 'cat', 'fish', $myarray);

// print_r() -- shows data and basic structure
// 
//  var_dump() -- shows data, data tipe and basic structure
//  
//  var_export -- the code required to re-create 

?><!DOCTYPE html>
<html>
<head>
	<title>DEbugging</title>
</head>
<body>
  <h1>Basic Debugging</h1>
  <h2>print_r</h2>
  <pre>

  <?php print_r($myarray) ?>

  <?php print_r ($myarray2) ?>

  </pre>
  <h2>var_dump</h2>

  <pre>
  <?php var_dump($myarray) ?>

  <?php var_dump ($myarray2) ?>
  </pre>
  
   <h2>var_export</h2>

  <pre>
  <?php var_export ($myarray) ?>

  <?php var_export ($myarray2) ?>
  </pre>
</body>
</html>