<?php

// Creating arrays
// 1 use the array keyword
// 
$fruits = array('apple', 'orange', 'banana');

// 2 usew literal array syntax
$pets = ['dog', 'cat', 'hamster'];

// 3 add elements to empty array
$cars = [];
$cars[] = 'Chevy';
$cars[] = 'BMW';
$cars[] = 'Honda';
// This is a sbhortcut for array_push(); // push smth to the end


// Loops 



?><!DOCTYPE html>
<html>
<head>
	<title>Arrays</title>
</head>
<body>
  <h1>Arrays</h1>
  
  <h2>Fruits output by key</h2>
  <ul>
	  <li><?=$fruits[0]?></li>
	  <li><?=$fruits[1]?></li>
	  <li><?=$fruits[2]?></li>
 </ul>

<h2>Pets by For loop</h2>
  <ul>
  	<?php
  	  for($i=0;$i<2;$i++){
  	  	echo "<li>{$pets[$i]}</li>";
  	  }
	  ?>
 </ul>

 <h2>Cars by Foreach Loop</h2>
  <ul>
  	<?php
  	  foreach($cars as $value){
  	  	echo "<li>{$value}</li>";
  	  }
	  ?>


 </ul>

 <h2>Cars by Foreach Loop</h2>
  <ul>
  	<?php
  	  foreach($cars as $key => $value){
  	  	echo "<li>$key: {$value}</li>";
  	  }
	  ?>


 </ul>

</body>
</html>