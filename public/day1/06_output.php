<?php

show_source(__FILE__);

$first_name = 'Maryna';
$last_name = "Haidashevska"

// the echo language contstruct can be used to output values 
// dont do output outside the HTML
// print() can be used for output  -- print is a function;
// WE can also output 
// 
?><!DOCTYPE html>
<html>
<head>
	<title>Output</title>
</head>
<body>
  <h1>PHP Output</h1>

  <p><?php echo $first_name; ?></p>
  <p><?php print($last_name); ?></p>
  <p><?=$first_name?></p>
  <p><?=$last_name?></p>
</body>
</html>