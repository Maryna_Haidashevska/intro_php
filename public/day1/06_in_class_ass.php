<?php

define('TITLE', 'Tullips');

$image = 'tullips.jpg';

$sentence1 = '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>';


$sentence2 = '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>'

?><!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" >
	<title>In class assignments</title>
</head>
<body>
  
  <div class="container">
  	<h1><?php echo TITLE?></h1>

  	<div class="row">

  		<div class="col col1">
  			<img src="<?php echo $image?>" />
  		</div>
  	</div>

  	<div class="row">

  		<div class="col col1">
  			<p><?=$sentence1?></p>
  		</div>
  	</div>

  	<div class="row">

  		<div class="col col1">
  			<p><?=$sentence2?></p>
  		</div>
  	</div>

  </div>


</body>
</html>