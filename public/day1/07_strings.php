<?php

$first = 'Hello, ';
$second = 'World';
$third = $first . $second; // Hello, World

// Two types of strings
// " " // double quotes
// ' ' // single quotes

$fname = 'Dave';
$lname = 'Jones';
$string1 = 'Hello! My name is $fname $lname. Nice to meet you';


$string2 = "Hello! My name is $fname $lname. NIce to meet you";

$string3 = "Hello! My name is {$fname} {$lname}. Nice to meet you";

$string4 = 'Hello, I am bill\'s dog';

$string5 = "Hello, I am bill's dog";

$string6 = "Hello, I am a fish \n named bill";
?><!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
  <h1>Strings</h1>
  <?=$string1?> <br />
  <?=$string2?> <br />
  <?=$string3?>
</body>
</html>